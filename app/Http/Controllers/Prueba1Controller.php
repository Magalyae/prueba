<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;

class Prueba1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Vticket = Ticket::all();
        return view('tickets.index',compact('Vticket'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tickets.newticktes');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = new Ticket;
        $ticket->codigo = $request->input('codigo');
        $ticket->nivel = $request->input('Nimportancia');
        $ticket->fecha = $request->input('fecha');
        $ticket->save();
        return redirect()->route('ticket.index')->with('info', 'Hemos recibido tu mensaje');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Vticket = Ticket::findOrfail($id);
        return view('tickets.edit', compact('Vticket'));
        // return "dentro de show".$Vticket ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Vticket = Ticket::findOrfail($id);
        return view('tickets.edit', compact('Vticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vticket = Ticket::findOrfail($id);
        // return $request;
        $Vticket->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return "el valor del id ".$id;
        Ticket::findOrfail($id)->delete();
        return redirect()->route('ticket.index');
    }
}
