@extends('layout')
@section('seccion')
<h2>Tickets Nro: {{ $Vticket->id }}</h2>
	<form method="POST" action="{{ route('ticket.update', $Vticket->id) }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        {{-- @csrf --}}
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="codigo">Codigo</label>
				<input type="text" class="form-control" id="codigo" name = "codigo" placeholder="Codigo" value = "{{ $Vticket->codigo }}">
			</div>
		</div>
		<div class="form-row ">
            <div class="form-group col-md-6">
                <label for="NImportancia">Nivel de importancia</label>
                <input type="text" class="form-control" name = "Nimportancia" id="Nimportancia" placeholder="Nivel de Importancia" value = "{{ $Vticket->nivel }}">
            </div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="fecha">Fecha </label>
				<input type="text" class="form-control" id="fecha" name="fecha" value = "{{ $Vticket->fecha }}">
			</div>
		</div>

		<button type="submit" class="btn btn-primary">Guardar</button>
	</form>
@endsection