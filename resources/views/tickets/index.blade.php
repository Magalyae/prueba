@extends('layout')
@section('seccion')

<div>
	<table class="table">
			{!! csrf_field() !!}
			{!! method_field('DELETE') !!}
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Codigo</th>
				<th scope="col">Nivel</th>
				<th scope="col">Fecha</th>
				<th><a class="btn btn-primary" href="{{ route('ticket.create') }}" role="button">Nuevo Ticket</a></th>
			</tr>
		</thead>
		<tbody>
			@foreach($Vticket as $tickets)
				<tr>
					<th scope="row">{{$tickets->id}}</th>
					<th >{{$tickets->codigo}}</th>
					<th >{{$tickets->nivel}}</th>
					<th >{{$tickets->fecha}}</th>
					<th><a class="btn btn-success" href="{{ route('ticket.edit', $tickets->id) }}" role="button">Modificar</a></th>
					<th>
						<form method = "POST" action="{{ route('ticket.destroy', $tickets->id) }}">
							@csrf
							@method('DELETE')
							
							<button class="btn btn-danger" type="submit">Eliminar</button>
						</form>
					</th>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection